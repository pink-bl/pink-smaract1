#!../../bin/linux-x86_64/sma

## You may have to change sma to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sma.dbd"
sma_registerRecordDeviceDriver pdbbase

# Configure each controller
#drvAsynIPPortConfigure("L1","172.17.10.33:5000",0,0,0)
drvAsynIPPortConfigure("L1","$(DEVIP):$(DEVPORT)",0,0,0)

# Controller port, asyn port, number of axis, moving poll period, idle poll period
# smarActMCSCreateController(const char *motorPortName, const char *ioPortName, int numAxes, double movingPollPeriod, double idlePollPeriod);
smarActMCSCreateController("P0", "L1", 15, 0.020, 1.0)

# Controller port, axis number, controller channel
# smarActMCSCreateAxis(const char *motorPortName, int axisNumber, int channel)
smarActMCSCreateAxis("P0", 0, 0);
smarActMCSCreateAxis("P0", 1, 1);
smarActMCSCreateAxis("P0", 2, 2);
smarActMCSCreateAxis("P0", 3, 3);
smarActMCSCreateAxis("P0", 4, 4);
smarActMCSCreateAxis("P0", 5, 5);
smarActMCSCreateAxis("P0", 6, 6);
smarActMCSCreateAxis("P0", 7, 7);
smarActMCSCreateAxis("P0", 8, 8);

smarActMCSCreateAxis("P0", 9, 9);
smarActMCSCreateAxis("P0", 10, 10);
smarActMCSCreateAxis("P0", 11, 11);

smarActMCSCreateAxis("P0", 12, 12);
smarActMCSCreateAxis("P0", 13, 13);
smarActMCSCreateAxis("P0", 14, 14);

## Load record instances
dbLoadRecords("$(ASYN)/db/asynRecord.db", "P=PINK:SMA01:, R=asyn, PORT=L1, ADDR=0, OMAX=256, IMAX=256")

cd "${TOP}/iocBoot/${IOC}"

## Load record template
dbLoadTemplate "motor.substitutions.smaractmcs"

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30, "P=PINK:SMA01")

## Start any sequence programs
#seq sncxxx,"user=epics"
